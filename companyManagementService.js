var dbApis = require('./dataBaseMysql.js');
var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var fileUpload = require('express-fileupload');
var fs = require("fs");
var csv = require("csvtojson");


var hostname = 'localhost';
var port = 3000;

var app = express();
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(fileUpload());

app.all('/companies', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'application/json'});
    next();
});

app.get('/companies', function(req, res, next){
    dbApis.getAllCompanies(function(companiesResponse){
        console.log(companiesResponse);
        res.end(JSON.stringify(companiesResponse));
    });
});

app.post('/companies', function(req, res, next){
    console.log(req.body);
    dbApis.addNewCompany(req.body.Company_name, req.body.Representative, req.body.Representative_email, req.body.Contact_Telephone, req.body.Address, function(companiesResponse){
        console.log(companiesResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.put('/companies', function(req, res, next){
    dbApis.updateCompany(req.body, function(companiesResponse){
        console.log(companiesResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.delete('/companies/:companyId', function(req, res, next){
    dbApis.deleteCompanyById(req.params.companyId, function(companiesResponse){
        console.log(companiesResponse);
        res.end('{"Company_id":'+ req.params.companyId + '}');
    });
});

app.all('/employees', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'application/json'});
    next();
});

app.get('/employees', function(req, res, next){
    dbApis.getAllEmployees(function(employeesResponse){
        console.log(employeesResponse);
        res.end(JSON.stringify(employeesResponse));
    });
});

app.post('/employees', function(req, res, next){
    console.log(req.body);
    dbApis.addNewEmployee(req.body.First_Name, req.body.Last_name, req.body.Job, req.body.Mail, req.body.Telephone, req.body.Company_id, function(employeesResponse){
        console.log(employeesResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.put('/employees', function(req, res, next){
    dbApis.updateEmployee(req.body, function(employeesResponse){
        console.log(employeesResponse);
        res.end(JSON.stringify(req.body));
    });
});

app.delete('/employees/:Employee_ID', function(req, res, next){
    dbApis.deleteEmployeeById(req.params.Employee_ID, function(employeesResponse){
        console.log(employeesResponse);
        res.end('{"Employee_ID":'+ req.params.Employee_ID + '}');
    });
});
app.post('/employeesFromFile', function(req, res, next){
  console.log(req.body);
  console.log(req.body.companyId);
  csv({
      delimiter: ';'
    })
  .fromString(req.body.message)
  .on('json', (employee)=>{
    console.log(employee);
      console.log(employee);
      dbApis.addNewEmployee(employee.firstName, employee.lastName, employee.newPosition, employee.newEmail, employee.newPhone, req.body.companyId, function(employeesResponse){
          console.log(employeesResponse);
          res.end(JSON.stringify(req.body));
      });
  })
  .on('done', (error) => {
    console.log('end csv to json');
    return res.status(500).send(error);
  })
  res.location('/static/employees.html');
  res.send(200, null);
});

app.use(express.static('/public'));
app.use('/static', express.static('public'))


app.listen(port, hostname, function(){
    console.log(`Server is running at http://${hostname}:${port}/`)
});
