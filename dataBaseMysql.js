var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'Parool11',
    database: 'companies'
});

connection.connect();

exports.addNewCompany = function(companyName, companyRep, repEmail, repPhone, compAddress, callback){
    connection.query("INSERT INTO companies (Company_name, Representative, Representative_email, Contact_Telephone, Address) VALUES ('" + companyName + "', '" + companyRep + "', '" + repEmail + "', '" + repPhone + "', '" + compAddress + "')", function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing query');
        }
    });
}

exports.getAllCompanies = function(callback) {
    connection.query('SELECT * FROM companies', function (err, rows, fields) {
        if(!err){
            return callback(rows);
        }else {
          console.log('Error on performing query');
        }
    });
};

exports.deleteCompanyById = function(companyId, callback) {
    connection.query('DELETE FROM companies WHERE Company_id = ' + companyId, function(err, rows, fields){
        if (!err) {
            return callback(rows);
        }else {
            console.log('Error on performing delete');
        }
    });
};

exports.updateCompany = function(companies, callback) {
    connection.query("UPDATE companies SET Company_name = '" + companies.Company_name + "', Representative = '" + companies.Representative + "', Representative_email = '" + companies.Representative_email + "', Contact_Telephone = '" + companies.Contact_Telephone + "', Address = '" + companies.Address + "'  WHERE Company_id = " + companies.Company_id, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing update query');
        }
    });
};

exports.addNewEmployee = function(firstName, lastName, position, employeeEmail, employeePhone, companyId, callback){
    connection.query("INSERT INTO employees (First_Name, Last_name, Job, Mail, Telephone, Company_id) VALUES ('" + firstName + "', '" + lastName + "', '" + position + "', '" + employeeEmail + "', '" + employeePhone + "', " + companyId + ")", function(err, rows, fields) {
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing query');
        }
    });
}

exports.getAllEmployees = function(callback) {
    connection.query('SELECT * FROM employees', function (err, rows, fields) {
        if(!err){
            return callback(rows);
        }else {
          console.log('Error on performing query');
        }
    });
};

exports.deleteEmployeeById = function(employeeId, callback) {
    connection.query('DELETE FROM employees WHERE Employee_ID = ' + employeeId, function(err, rows, fields){
        if (!err) {
            return callback(rows);
        }else {
            console.log('Error on performing delete');
        }
    });
};

exports.updateEmployee = function(employees, callback) {
    connection.query("UPDATE employees SET First_Name = '" + employees.First_Name + "', Last_name = '" + employees.Last_name + "', Job = '" + employees.Job + "', Mail = '" + employees.Mail + "', Telephone = '" + employees.Telephone + "'  WHERE Employee_ID = " + employees.Employee_ID, function(err, rows, fields){
        if(!err){
            return callback(rows);
        } else {
            console.log('Error on performing update query');
        }
    });
};
