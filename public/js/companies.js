var companies;
function fillAllCompaniesTable() {
    console.log("funktsioon läks käima");
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/companies",
        success: function(companiesResponse){
            companies = companiesResponse;
            var tableContent = "";
            console.log(companiesResponse);

            for(var i = 0; i < companiesResponse.length; i++) {
                tableContent = tableContent + '<tr><td>' + (i+1) + '</td><td>'
                    + companiesResponse[i].Company_name +
                    '</td><td>' + companiesResponse[i].Representative + '</td><td>' + companiesResponse[i].Representative_email + '</td><td>' + companiesResponse[i].Contact_Telephone + '</td><td>' + companiesResponse[i].Address + '</td><td><input class="btn btn-default modify-companY" type="button" value="Muuda" data-toggle="modal" data-target="#modifyCompanyModal"   onClick ="openModalWithData(' +
                    + companiesResponse[i].Company_id
                    + ')"><input class="btn btn-default delete-companies" type="button" value="Kustuta" data-toggle="modal" data-target="#deleteCompanyModal"   onClick ="openDeleteCompanyConfirmModal(' +
                    + companiesResponse[i].Company_id
                    + ')"></td></tr>';
            }
            document.getElementById("companiesTable").innerHTML = tableContent;
        }
    });
}


function addNewCompany() {
    var newCompany = document.getElementById("newCompanyName").value;
    var newRep = document.getElementById("newCompanyRep").value;
    var newEmail = document.getElementById("newRepEmail").value;
    var newPhone = document.getElementById("newRepPhone").value;
    var newAddress = document.getElementById("newCompanyAddress").value;
    if(newCompany != "") {
        var addCompany = JSON.stringify({"Company_name": newCompany, "Representative": newRep, "Representative_email": newEmail, "Contact_Telephone": newPhone, "Address": newAddress});
        console.log(addCompany);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: addCompany,
            url: "http://localhost:3000/companies",
            success: function(companiesResponse){
                console.log(companiesResponse);
                fillAllCompaniesTable();
                showAlert("success-alert");
            }
        });
    }else{
        //showAlert("danger-alert");
        $("#danger-alert").alert();
        $("#danger-alert").fadeTo(2000, 500);
    }
}

function showAlert(alertId){
    $("#"+alertId).alert();
    $("#"+alertId).fadeTo(2000, 500).slideUp(500, function(){
        $("#"+alertId).slideUp(500);
    });
}

function saveCompanyChanges() {
    var companyId = $(".modal-body #modifyCompanyId").val();
    var companyName = $(".modal-body #modifyCompanyName").val();
    var companyRep = $(".modal-body #modifyCompanyRep").val();
    var companyRepEmail = $(".modal-body #modifyCompanyRepEmail").val();
    var companyRepPhone = $(".modal-body #modifyCompanyRepPhone").val();
    var companyAddress = $(".modal-body #modifyCompanyAddress").val();
    if(companyName != ""){
        var modifiedCompany = JSON.stringify({"Company_id": companyId, "Company_name": companyName, "Representative": companyRep, "Representative_email": companyRepEmail,
      "Contact_Telephone": companyRepPhone, "Address": companyAddress});
        console.log(modifiedCompany);
        $.ajax({
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            data: modifiedCompany,
            url: "http://localhost:3000/companies",
            success: function(companiesResponse){
                console.log(companiesResponse);
                fillAllCompaniesTable();
                $('#modifyCompanyModal').modal('hide');
                showAlert("modify-success-alert");
            }
        });
    }else{
        showAlert("modify-danger-alert");
    }
}

function openModalWithData(selectedCompanyId) {
            for(var i= 0; i<companies.length; i++){
                if(companies[i].Company_id == selectedCompanyId){
                    $(".modal-body #modifyCompanyId").val(selectedCompanyId);
                    $(".modal-body #modifyCompanyName").val(companies[i].Company_name);
                    $(".modal-body #modifyCompanyRep").val(companies[i].Representative);
                    $(".modal-body #modifyCompanyRepEmail").val(companies[i].Representative_email);
                    $(".modal-body #modifyCompanyRepPhone").val(companies[i].Contact_Telephone);
                    $(".modal-body #modifyCompanyAddress").val(companies[i].Address);
                }
            }
};
var companyToDeleteId;
var companyToDeleteName;
function openDeleteCompanyConfirmModal(selectedCompanyId) {
    for(var i = 0; i < companies.length; i++){
        if(companies[i].Company_id == selectedCompanyId){
          companyToDeleteId = selectedCompanyId;
          companyToDeleteName = companies[i].Company_name;
          $(".modal-body #deleteMessage").text("Kas oled kindel, et soovid kustutada firma " +companyToDeleteName +"?");
        }
    }
}


function deleteCompany() {
    var companyId = companyToDeleteId
    if(companyToDeleteId != null){
        $.ajax({
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            url: "http://localhost:3000/companies/" + companyToDeleteId,
            success: function(companiesResponse){
                console.log(companiesResponse);
                fillAllCompaniesTable();
                $('#deleteCompanyModal').modal('hide');
                showAlert("delete-success-alert");
            }
        });
    }
}
function deleteModalWithData(selectedCompanyId) {
            for(var i= 0; i<companies.length; i++){
                if(companies[i].Company_id == selectedCompanyId){
                    $(".modal-body #modifyCompanyId").val(selectedCompanyId);
                    $(".modal-body #modifyCompanyName").val(companies[i].Company_name);
                }
            }
};
