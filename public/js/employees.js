var employees;
function fillAllEmployeesTable() {
    console.log("funktsioon läks käima");
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/employees",
        success: function(employeesResponse){
            employees = employeesResponse;
            var tableContent = "";
            console.log(employeesResponse);

            for(var i = 0; i < employeesResponse.length; i++) {
                tableContent = tableContent + '<tr><td>' + (i+1) + '</td><td>'
                    + employeesResponse[i].Company_id +'</td><td>'
                    + employeesResponse[i].First_Name +
                    '</td><td>' + employeesResponse[i].Last_name + '</td><td>' + employeesResponse[i].Job + '</td><td>' + employeesResponse[i].Mail + '</td><td>' + employeesResponse[i].Telephone + '</td><td><input class="btn btn-default modify-employee" type="button" value="Muuda" data-toggle="modal" data-target="#modifyEmployeeModal"   onClick ="openModalWithData(' +
                    + employeesResponse[i].Employee_ID
                    + ')"><input class="btn btn-default delete-employees" type="button" value="Kustuta" data-toggle="modal" data-target="#deleteEmployeeModal"   onClick ="openDeleteEmployeeConfirmModal(' +
                    + employeesResponse[i].Employee_ID
                    + ')"></td></tr>';
            }
            document.getElementById("employeesTable").innerHTML = tableContent;
        }
    });
}

var companies;
function companySelectMenu() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "http://localhost:3000/companies",
        success: function(companiesResponse){
            companies = companiesResponse;
            console.log(companiesResponse);
            var options = "";
            var placeholder = '<option value="" aria-required="true" selected disabled class="text-hide">Vali firma rippmenüüst</option>'
            for(var i = 0; i < companiesResponse.length; i++) {
                options = options + '<option value="' + companiesResponse[i].Company_id + '">' + companiesResponse[i].Company_name
                +  '</option>';
            }
            var companyList = placeholder + options
            document.getElementById("cSM").innerHTML = companyList;
            document.getElementById("cSMModal").innerHTML = companyList;
        }
    });
}

function addEmployeesFile() {
    var file = document.getElementById("employeesFile").files[0];
    var reader = new FileReader();
    reader.readAsText(file, 'ISO-8859-4');
    reader.onload = loaded;
    reader.onerror = errorHandler;
}

function loaded(event) {
    var fileString = event.target.result;
    var company = $("#cSMModal :selected").val();
    console.log(fileString);
    if(fileString != "") {
      $.ajax({
           type: "POST",
           dataType: "json",
           contentType: "application/json",
           url: "http://localhost:3000/employeesFromFile",
           data: JSON.stringify({"message" : fileString, "companyId" : company}),
           success: function(response) {
             console.log(response);
             $('#addEmployeesModal').modal('hide');
             fillAllEmployeesTable();
             showAlert("file-success-alert");
            }
        });
    }
}

function errorHandler(event) {
  console.log("error occured!");
}


function addNewEmployee() {
    var firstName = document.getElementById("newFirstName").value;
    var lastName = document.getElementById("newLastName").value;
    var newPosition = document.getElementById("newPosition").value;
    var newEmail = document.getElementById("newEmail").value;
    var newPhone = document.getElementById("newPhone").value;
    var companyId = $("#cSM :selected").val();

    if(firstName !="" && lastName !="" && companyId !="") {
        var addEmployee = JSON.stringify({"First_Name": firstName, "Last_name": lastName, "Job": newPosition, "Mail": newEmail, "Telephone": newPhone, "Company_id": companyId});
        console.log(addEmployee);
        console.log(companyId);
        $.ajax({
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: addEmployee,
            url: "http://localhost:3000/employees",
            success: function(employeesResponse){
                console.log(employeesResponse);
                fillAllEmployeesTable();
                showAlert("success-alert");
            }
        });
    }else{
        $("#danger-alert").alert();
        $("#danger-alert").fadeTo(2000, 500);
    }
}

function showAlert(alertId){
    $("#"+alertId).alert();
    $("#"+alertId).fadeTo(2000, 500).slideUp(500, function(){
        $("#"+alertId).slideUp(500);
    });
}

function saveEmployeeChanges() {
    var employeeId = $(".modal-body #modifyEmployeeId").val();
    var firstName = $(".modal-body #modifyFirstName").val();
    var lastName = $(".modal-body #modifyLastName").val();
    var position = $(".modal-body #modifyPosition").val();
    var email = $(".modal-body #modifyEmployeeEmail").val();
    var phone = $(".modal-body #modifyEmployeePhone").val();
    if(firstName != "" && lastName != ""){
        var modifiedEmployee = JSON.stringify({"Employee_ID": employeeId, "First_Name": firstName, "Last_name": lastName, "Job": position, "Mail": email, "Telephone": phone});
        console.log(modifiedEmployee);
        $.ajax({
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            data: modifiedEmployee,
            url: "http://localhost:3000/employees",
            success: function(employeesResponse){
                console.log(employeesResponse);
                fillAllEmployeesTable();
                showAlert("modify-success-alert");
                $('#modifyEmployeeModal').modal('hide');

            }
        });
    }else{
        showAlert("modify-danger-alert");
    }
}

function openModalWithData(selectedEmployeeId) {
    for(var i= 0; i<employees.length; i++){
        if(employees[i].Employee_ID == selectedEmployeeId){
            $(".modal-body #modifyEmployeeId").val(selectedEmployeeId);
            $(".modal-body #modifyFirstName").val(employees[i].First_Name);
            $(".modal-body #modifyLastName").val(employees[i].Last_name);
            $(".modal-body #modifyPosition").val(employees[i].Job);
            $(".modal-body #modifyEmployeeEmail").val(employees[i].Mail);
            $(".modal-body #modifyEmployeePhone").val(employees[i].Telephone);
        }
    }
};
var employeeToDeleteId;
var employeeToDeleteName;
function openDeleteEmployeeConfirmModal(selectedEmployeeId) {
    for(var i = 0; i < employees.length; i++){
        if(employees[i].Employee_ID == selectedEmployeeId){
          employeeToDeleteId = selectedEmployeeId;
          employeeToDeleteName = (employees[i].First_Name + " " + employees[i].Last_name);
          $(".modal-body #deleteMessage").text("Kas oled kindel, et soovid kustutada töötaja " + employeeToDeleteName +"?");
        }
    }
}


function deleteEmployee() {
    var employeeId = employeeToDeleteId
    if(employeeToDeleteId != null){
        $.ajax({
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            url: "http://localhost:3000/employees/" + employeeToDeleteId,
            success: function(employeesResponse){
                console.log(employeesResponse);
                fillAllEmployeesTable();
                $('#deleteEmployeeModal').modal('hide');
                showAlert("delete-success-alert");
            }
        });
    }
}
function deleteModalWithData(selectedEmployeeId) {
            for(var i= 0; i<employees.length; i++){
                if(employees[i].Employee_ID == selectedEmployeeId){
                    $(".modal-body #modifyEmployeeId").val(selectedEmployeeId);
                    $(".modal-body #modifyFirstName").val(employees[i].First_Name);
                    $(".modal-body #modifyLastName").val(employees[i].Last_name);
                }
            }
};
